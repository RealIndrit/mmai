class Default:
    def __init__(self):
        self.dataset = {'output': 'test.data', 'rescale_params': [200, 200],
                        'sharpening_param': 'grayscale_default', 'normalize': 255}
