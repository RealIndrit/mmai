import numpy as np
from utils.error.mmaiExceptions import MMAIException


class Algorithms:

    def image_sharpening(self, algorithm=None):
        if algorithm is None:
            algorithm = 'grayscale_default'

        if isinstance(algorithm, str):
            if algorithm is 'grayscale_default':
                return np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])

            elif algorithm is 'grayscale_hard':
                return np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])

        elif isinstance(algorithm, list):
            return np.array(algorithm)
        else:
            raise MMAIException('Unsupported object, expected type (list, str)', 'algorithm error')
