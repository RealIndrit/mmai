from utils.error.mmaiExceptions import MMAIException
from utils.tensor.network.connection import Connection
from utils.tensor.utils.activator import Activator


class Neuron:
    def __init__(self, layer, activator=None, threshold=None, disabled=False, eta=0.001, alpha=0.01):
        self.layerIn = layer
        self.activator = activator
        self.threshold = threshold
        self.disabled = disabled
        self.dendrites = self.getConnections()  # connections (Biology term)
        self.input = self.getInput()
        self.error = 0.0
        self.gradient = 0.0
        self.output = 0.0
        self.eta = eta
        self.alpha = alpha

    def feedForward(self):
        if self.activator is not None:
            if self.activator is 'custom':
                raise MMAIException("custom activator types is not supported yet", "YEEEET")
            else:
                self.output = Activator(self.input).activate(self.activator)
        else:
            self.output = self.input

        # Add later when dropout logic has been made functional (A lot of array handling, time for matrices?)

        if self.threshold is not None:
            if not self.withinThreshold(self.output):
                self.disabled = True

        if self.disabled:
            self.output = 0  # Basically disable this neuron... Will send 0 * weight of dendrite --> 0 in Sigma function

    def withinThreshold(self, output):
        # ['low': Num, 'high': Num]
        if 'low' in self.threshold:
            if self.threshold['low'] > output:
                return False
        if 'high' in self.threshold:
            if self.threshold['high'] < output:
                return False
        return True

    def addError(self, err):
        self.error = self.error + err

    def setError(self, err):
        self.error = err

    def setOutput(self, output):  # Let us do fun stuff with neurons in the future
        self.output = output

    def getOutput(self):
        return self.output

    def getInput(self):
        tot = 0
        if self.dendrites is None:  # No weight layer before it, first layer (input layer)
            return
        for dendrite in self.dendrites:
            tot = tot + dendrite.connectedNeuron.getOutput() * dendrite.weight
        return tot

    def getConnections(self):
        connections = []
        if self.layerIn is None:
            return connections
        else:
            for neuron in self.layerIn:
                connections.append(Connection(neuron))  # <-- Custom random params can be passed, (Activator has to be
                # compatible with weight values)
            return connections

    def backPropagate(self):
        if self.activator is not None:
            if self.activator is 'custom':
                raise MMAIException("custom activator types is not supported yet", "YEEEET")
            else:
                self.gradient = self.error * Activator(self.output).derivative(self.activator)
        else:
            self.gradient = self.error * self.output
        for dendrite in self.dendrites:
            dendrite.dWeight = self.eta * (
                    dendrite.connectedNeuron.output * self.gradient) + self.alpha * dendrite.dWeight
            dendrite.weight = dendrite.weight + dendrite.dWeight
            dendrite.connectedNeuron.addError(dendrite.weight * self.gradient)
        self.error = 0.0
