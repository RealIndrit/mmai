from utils.error.mmaiExceptions import MMAIException
from utils.tensor.network.networkparams import NetworkParams
from utils.tensor.network.neuron import Neuron
import numpy as np


class Network:
    def __init__(self):
        self.layers = []

    def build(self, params=None):
        if params is None:
            params = NetworkParams(params).parse()['network']
        for layer_index in params:
            layer = []
            for neuron_index in range(layer_index['nodes']):
                if len(self.layers) == 0:  # First layer can never have weights if not RNN
                    non_prelayer_neuron = Neuron(None, activator=layer_index['activation'])
                    layer.append(non_prelayer_neuron)
                else:
                    prelayer_neuron = Neuron(self.layers[-1], activator=layer_index['activation'])
                    layer.append(prelayer_neuron)
            self.layers.append(layer)  # MAYOR :thonk: here arrays are funky, please look into this o.o

    # TODO: Make a build method that uses all parameters when creating network,
    #  + actually get the neural network to work, which it doesnt atm...

    def setInput(self, inputs, range_filling=False, fill_type=-1):
        if range_filling is True:
            index = 0
            if len(inputs) < len(self.layers[0]):
                for i in range(len(inputs)):
                    self.layers[0][i].setOutput(inputs[i])
                    index = i
                for i in range(index + 1, len(self.layers[0])):
                    self.layers[0][i].setOutput(fill_type)

            if len(inputs) > len(self.layers[0]):
                for i in range(len(self.layers[0])):
                    self.layers[0][i].setOutput(inputs[i])

        elif len(inputs) > len(self.layers[0]):
            raise MMAIException("Array sizes does not match!", "Index out of range (too large)")
        elif len(inputs) < len(self.layers[0]):
            raise MMAIException("Array sizes does not match!", "Index below allowed input size")
        else:
            for i in range(len(inputs)):
                self.layers[0][i].setOutput(inputs[i])

    def feedForward(self):
        for layer in self.layers[1:]:
            for neuron in layer:
                neuron.feedForward()

    def backPropagate(self, target):
        for i in range(len(target)):
            self.layers[-1][i].setError(target[i] - self.layers[-1][i].getOutput())
        for layer in self.layers[::-1]:
            for neuron in layer:
                neuron.backPropagate()

    def getError(self, target):
        err = 0
        for i in range(len(target)):
            e = (target[i] - self.layers[-1][i].getOutput())
            err = err + e ** 2
        err = err / len(target)
        err = np.sqrt(err)
        return err

    def getResults(self):
        output = []
        for neuron in self.layers[-1]:
            output.append(neuron.getOutput())
        output.pop()
        return output

    np.random.rand()

    # TODO: Add save/load...
