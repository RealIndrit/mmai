import numpy as np


class Connection:
    def __init__(self, connectedNeuron, random=None, seed=None):
        if random is None:
            random = {'low': 0, 'high': 1}
        if seed is not None:
            np.random.seed(seed)
        self.connectedNeuron = connectedNeuron
        self.weight = float(np.random.randint(random['low'], random['high']))
        self.dWeight = 0.0

        # Should anything more be added?
