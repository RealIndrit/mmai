from utils.error.mmaiExceptions import MMAIException


class NetworkParams:
    def __init__(self, params=None):
        self.params = params
        self.required = {'network', 'specific'}
        self.default = {
            'network': {
                'global': {'type': 'dense', 'seed': 255, 'threshold': {'low': 0, 'high': 1}},
                'specific': [
                    {'nodes': 3, 'bias': True, 'activation': 'logistic'},
                    {'nodes': 2, 'bias': True, 'activation': 'relu'},
                    {'nodes': 1, 'activation': 'binarystep'}
                ]
            }
        }
        self.validateInput()

        # TODO: Make it check if all required parameters are in the build params,
        #  if so, parse them to builder readable format...

    def parse(self):
        if self.params is None:
            self.params = self.default
        network_array = []
        global_array = []
        network_params = self.params['network']

        if 'global' in network_params:
            for i in network_params['global']:
                global_array.append(i)
            print(global_array)

        output = {
            'network': [
                {'nodes': 3, 'bias': True, 'threshold': {'low': 0, 'high': 1}, 'seed': 255, 'activation': 'logistic',
                 'type': 'dense'},
                {'nodes': 4, 'bias': True, 'threshold': {'low': 0, 'high': 1}, 'seed': 255, 'activation': 'logistic',
                 'type': 'dense'},
                {'nodes': 1, 'threshold': {'low': 0, 'high': 1}, 'seed': 255, 'activation': 'logistic', 'type': 'dense'}
            ]
        }
        return {'network': network_array}

    def getDefault(self):
        return self.default

    def validateInput(self):
        if self.params is None:
            pass  # Literally do nothing, params will be use built in default params...
        else:
            if not 'network' in self.params:
                raise MMAIException("Network parameter is missing in build parameters", "Missing critical \"network\" "
                                                                                        "parameters")
            if not 'specific' in self.params['network']:
                raise MMAIException("Specific parameter is missing in build parameters", "Missing critical "
                                                                                         "\"specific\" "
                                                                                         "parameters")
            if not isinstance(self.params['network']['specific'], list):
                raise MMAIException("Specific is wrong type",
                                    "Specific is not list")
            else:
                for i in self.params['network']['specific']:
                    if not 'nodes' in i:
                        raise MMAIException("Nodes parameter is missing in build parameters",
                                            "Missing critical \"nodes\" parameters")
