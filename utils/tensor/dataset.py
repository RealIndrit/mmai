import os
import time

from utils.default import Default
from utils.filehandler import save, load, getImages
from libs.image.image import Image
from libs.image.utils import getHash


class TrainingSet:
    def __init__(self, run_tensor=False):
        # Temporary memory for allocating data from files or whatever until complete data set is built..
        self.data_array = {}
        self.run_tensor = run_tensor

    def build_data(self, img_data_directory, params=None):

        if params is None:
            params = Default().dataset
        folder = getImages(img_data_directory)
        img_data = []
        amount = len(folder)

        for f in folder:
            d = os.path.join(img_data_directory, f)
            img = Image(d)
            if 'sharpening_param' in params:
                img.sharpen(algorithm=params['sharpening_param'])

            if 'rescale_params' in params:
                img.compress(width=params['rescale_params'][0], height=params['rescale_params'][1])

            if 'normalize' in params:
                img.normalize(params['normalize'])

            img_data.append(img.getData())

        data = {
            'training_data': img_data,
            'date_created': time.time(),
            'img_count': amount,
            'hash': [getHash(img_data), 'SHA-256'],
            'params': params
        }

        self.data_array = data

        if 'output' not in params:
            print("Data was not saved.")
            return self.data_array
        else:
            save(params['output'], self.data_array)
            print(f"Data was saved: {params['output']}")
            return None

    def load_data(self, dir, return_data=False):
        self.data_array = load(dir)
        if return_data:
            return self.data_array
