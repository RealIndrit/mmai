import numpy as np


class Loss:
    def __init__(self, output, exc):
        self.output = output
        self.expected = exc

    def loss(self, function):
        method_to_call = getattr(self, function)
        return method_to_call()

    def MSE(self):
        return np.sqrt(np.sum([np.power(i - _i) for i, _i in zip(self.expected, self.output)])) # <-
        # - Proudly stolen from google, god loves numpy

