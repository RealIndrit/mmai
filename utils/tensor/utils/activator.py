import numpy as np


class Activator:
    def __init__(self, input, const=1):
        self.input = input
        self.const = const

    def derivative(self, function):
        method_to_call = getattr(self, function + "_derivative")
        return method_to_call()

    def activate(self, function):
        method_to_call = getattr(self, function)
        return method_to_call()

    def binarystep(self):
        return np.maximum(0, 1)

    def logistic(self):
        return self.const / (1 + np.exp(-self.input))

    def relu(self):
        return np.maximum(0, self.input)

    def logistic_derivative(self):
        sig = self.logistic()
        return sig/(1 - sig)

    def relu_derivative(self):
        dZ = np.array(self.const, copy=True)
        dZ[self.input <= 0] = 0
        return dZ