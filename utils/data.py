import csv


def circ2rec2p(data):

    """
    # Arguments
        data      : array of circle data in an array as [X,Y,D](X is x cord, Y is y cord and D is diameter)
    """

    x1 = data[0] - data[2]/2
    y1 = data[1] + data[2]/2
    x2 = data[0] + data[2]/2
    y2 = data[1] - data[2]/2
    recData = [str(int(x1)), str(int(y1)), str(int(x2)), str(int(y2))]

    return recData


def extractCSV(dir):

    """
    # Arguments
        dir      : The directory of the CSV data file
    """

    data = []
    with open(dir) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            data.append([float(row[0]), float(row[1]), float(row[2])])  # X, Y, D

    return data


def scalecord(facX, facY, cords):

    """
    # Arguments
        facX        : The scale factor the cordinate(s) is going to be scaled
        facY        : The scale factor the cordinate(s) is going to be scaled
        cords       : An array of cordinate(s)
    """

    scaled = [int(cords[0]*facX), int(cords[1]*facY)]
    return scaled


