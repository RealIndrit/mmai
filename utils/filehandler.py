import os
import pickle
import shutil
import requests


def download(url, dir=None):
    """
    # Arguments
       url             : The url where the file(s) is supposedly found
       dir             : The directory where the file(s) is saved
    """

    r = requests.get(url)
    if dir is not None:
        with open(dir, 'wb') as f:
            f.write(r.content)
    else:
        return r.content


def getImages(dir):
    """
    # Arguments
        dir             : The directory where the file(s) is found
    """

    validfiles = [f for f in os.listdir(dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]

    return validfiles


def getValFiles(dir):
    """
    # Arguments
        dir             : The directory where the file(s) is found
    """

    validfiles = [f for f in os.listdir(dir) if f.lower().endswith('val')]
    return validfiles


def rename(dir, name, newname):
    """
    # Arguments
        dir             : Base path, the complete parent directory to file
        name            : The name of the file you want to rename
        newname         : The new name for the given file
    """

    os.rename(os.path.join(dir, name), os.path.join(dir, newname))

    return  # Maybe we will return something in the future? Let it be for now I guess...


def move(dir, newdir):
    """
    # Arguments
        dir      : Original directory of file you want to move
        newDir   : Target directory
    """

    shutil.move(dir, newdir)

    return


def delete(dir):
    """
    # Arguments
        dir      : Directory of file you want to remove
    """

    os.remove(dir)

    return


def clearAll(dirin):
    """
    # Arguments
        dir      : Directory which is going to be emptied
    """

    for f in os.listdir(dirin):
        dir = os.path.join(dirin, f)
        delete(dir)

    return


def getName(dir):
    """
    # Arguments
       dir      : Directory which string of name before first "." is fetched
    """

    fileArr = dir.split('.')

    return fileArr[0]


def isURL(input):
    return os.path.islink(input)


def isFile(input):
    return os.path.isfile(input)


def isDir(input):
    return os.path.isdir(input)


def getLastDirectory(input):
    """
       # Arguments
          dir   : Directory which string of name after last "/" in directory/url is fetched
    """

    split = input.split('/')
    file = split.pop()  # Get last index of array...

    return str(file)


def load(dir, type="rb"):
    """
    :param dir:
    :param data:
    :param type:
    :return:
    """
    return pickle.load(open(dir, type))


def save(dir, data, type="wb"):
    """
    # Arguments
       dir      : The directory where the data is saved/replaced
       data     : The data being saved, can be whatever typ of data (array or strings are recommended)
       type     : The way the save will be done, "a" = appending, "w+" = write over etc, see google for more
    """
    print("Saving...")
    with open(dir, type) as file:
        pickle.dump(data, file)
    return
