import os
import random
from collections import deque

import numpy as np
from utils.error.mmaiExceptions import MMAIException
import pandas as pd
from sklearn import preprocessing


class Stockmarket:

    def __init__(self, params=None):

        if params is None:
            raise MMAIException('Empty parameter', 'Parameters was not entered')

        elif not isinstance(params['data'], str):
            raise MMAIException('Wrong type for data type', 'Expected type "file"')

        elif not isinstance(params, dict):
            raise MMAIException('Wrong type for params parameter', 'Expected type "list"')

        else:
            self.params = params
            self.main_dataframe = pd.DataFrame()
            self.validation_main_dataframe = pd.DataFrame()
            self.SEQ_LEN = 60
            self.FPP = 3
            self.RTP = "LTC-USD"

    def preprocess(self, df):
        df = df.drop('future', 1)

        for col in df.columns:
            if col != 'target':
                df[col] = df[col].pct_change()
                df.dropna(inplace=True)
                df[col] = preprocessing.scale(df[col].values)

        df.dropna(inplace=True)

        seq_data = []
        prev_days = deque(maxlen=self.SEQ_LEN)

        for i in df.values:
            prev_days.append([n for n in i[:-1]])
            if len(prev_days) == self.SEQ_LEN:
                seq_data.append([np.array(prev_days), i[-1]])
        random.shuffle(seq_data)
        buys = []
        sells = []

        for seq, target in seq_data:
            if target == 0:
                sells.append([seq, target])
            elif target == 1:
                buys.append([seq, target])

        random.shuffle(buys)
        random.shuffle(sells)

        lower = min(len(buys), len(sells))

        buys = buys[:lower]
        sells = sells[:lower]

        sequential_data = buys + sells
        random.shuffle(
            sequential_data)

        X = []
        y = []

        for seq, target in sequential_data:
            X.append(seq)
            y.append(target)

        return np.array(X), y

    def buy_sell(self, current, future):
        if float(future) > float(current):
            return 1
        else:
            return 0

    def create_validation(self):
        times = sorted(self.main_dataframe.index.values)
        validation_period = times[-int(0.05 * len(times))]

        self.validation_main_dataframe = self.main_dataframe[(self.main_dataframe.index >= validation_period)]
        self.main_dataframe = self.main_dataframe[(self.main_dataframe.index < validation_period)]

    def predict_target(self):
        self.main_dataframe['future'] = self.main_dataframe[f'{self.RTP}_close'].shift(-self.FPP)
        self.main_dataframe['target'] = list(map(self.buy_sell,
                                                 self.main_dataframe[f'{self.RTP}_close'],
                                                 self.main_dataframe['future']))

    def load_merge(self):
        # If the option was made to give the fields names
        if self.params['rename_field_names'] is not None:
            if isinstance(self.params['rename_field_names'], list):
                if os.path.isdir(self.params['data']):
                    for file in os.listdir(self.params['data']):
                        file_name = os.fsdecode(file)

                        if file_name.endswith('.csv'):
                            data = pd.read_csv(os.path.join(self.params['data'], file_name),
                                               names=self.params['rename_field_names'])
                            file_name = file_name.split('.')[0]
                            data.rename(columns={'close': f'{file_name}_close', 'volume': f'{file_name}_volume'},
                                        inplace=True)
                            data.set_index('time', inplace=True)
                            data = data[[f'{file_name}_close', f'{file_name}_volume']]

                            if len(self.main_dataframe) == 0:
                                self.main_dataframe = data
                            else:
                                self.main_dataframe = self.main_dataframe.join(data)
                        else:
                            # If file does not end with .csv skipp it.
                            print('File had inaccurate ending... Skipped!')
                else:
                    # if data entered is not a directory
                    # Do Stuff
                    print()

            else:
                raise MMAIException('Wrong type', 'Expected list')
        else:
            pass

        self.predict_target()
        self.create_validation()
        x_train, y_train = self.preprocess(self.main_dataframe)
        x_validation, y_validation = self.preprocess(self.validation_main_dataframe)

        return (x_train, y_train), (x_validation, y_validation)
