import hashlib
from utils.algorithms.algorithms import Algorithms as algo
import numpy as np
import cv2


def drawRect(image, dirout, k):

    """
    # Arguments
        image               : The primary image object
        dirout              : The output directory
        k                   : Array of cordinates [x1,y1,x2,y2]
    """
    img = image.img_array
    cv2.rectangle(img, (k[0], k[1]), (k[2], k[3]), (0, 0, 255), 1)
    cv2.imwrite(dirout, img)


def showImage(image_array, name="Image", seconds=10):
    cv2.imshow(name, image_array)
    cv2.waitKey(seconds*1000)
    cv2.destroyAllWindows()
    return


def sharpenImage(image_array, sharpening_algorithm='grayscale_default'):

    """
    # Arguments
       image                : The image object which is being sharpened
       dirout               : The output directory

    # See http://web.pdx.edu/~jduh/courses/Archive/geog481w07/Students/Ludwig_ImageConvolution.pdf
    """

    kernel_sharpening = algo().image_sharpening(sharpening_algorithm)

    sharpened = cv2.filter2D(image_array, -1, kernel_sharpening)
    return sharpened


def getHash(data):
    # Prepare the project id hash
    hashId = hashlib.sha256()

    hashId.update(repr(data).encode('utf-8'))

    return hashId.hexdigest()


def getScaling(image1, image2):

    """
    # Arguments
       image1               : The primary image object
       image2               : The secondary image object
    """

    scaleX = image1.width / image2.width
    scaleY = image1.height / image2.height

    return scaleX, scaleY


def resize(image_array, width, height):  # 450 is normal moon image width

    """
    # Arguments
        image               : The image object that is being resized
        scaleX              : Scale image in X axis (Width on the sharpen image)
        scaleY              : Scale image in Y axis (Height on the sharpen image) (if None its same as X)
    """
    new_img_array = cv2.resize(image_array, (width, height))
    return new_img_array


def byte2img(array):
    img = np.fromstring(array, dtype=np.uint8)
    return img