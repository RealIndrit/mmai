import cv2
from collections import Counter
from sklearn.cluster import KMeans
import numpy as np

from utils.error.mmaiExceptions import MMAIException
from utils.filehandler import getLastDirectory, download, isURL
from libs.image.utils import sharpenImage, resize, byte2img

np.set_printoptions(threshold=np.inf)


# noinspection PyAttributeOutsideInit
class Image:
    def normalize(self, const):
        self.normalizeConst = const
        self.normalized = True
        self.init()
        pass

    def sharpen(self, algorithm=None):
        self.img_array = sharpenImage(self.img_array, sharpening_algorithm=algorithm)
        self.sharpened = True
        self.init()
        return

    def compress(self, width=150, height=150):
        self.img_array = resize(self.img_array, width, height)
        self.compressed = True
        self.init()
        return

    def toArray(self):
        if isURL(self.dir):
            raise MMAIException('Data from remote repository is not supported yet', 'remote data')
            x = download(self.dir)
            img = byte2img(x)
            img = cv2.imdecode(img, cv2.COLOR_RGB2GRAY)
            return img
        else:
            return cv2.imread(self.dir, cv2.IMREAD_GRAYSCALE)  # cv2.imread returns a numpy array by default

    def imageValidation(self):
        # Not needed atm, re enable later...
        """if not (self.dominant in range(50, 130)) and \
                (self.high in range(60, 180)) and \
                (self.low in range(0, 30)):
            return False"""

        return True

    def getdominant(self, k=5):
        # reshape the image to be a list of pixels, and also decrease the amount of pixels (faster computation times)
        image = cv2.resize(self.img_array, (45, 45))  # hardcoded values.
        image = image.reshape((image.shape[0] * image.shape[1], 1))

        # cluster and assign labels to the pixels (lesser amount of clusters ==> faster)
        clt = KMeans(n_clusters=k)
        labels = clt.fit_predict(image)

        # count labels to find most popular
        label_counts = Counter(labels)

        # subset out most popular centroid
        dominant_color = clt.cluster_centers_[label_counts.most_common(1)[0][0]]

        return dominant_color

    def getlowestpixel(self):
        """
        # Arguments
            dir             : The directory where the file(s) is found
        # Extra
            Treats it as an array of graph-points checks for lowest values
        """

        img = self.img_array[:, :-1]
        smallest = img.min(axis=0).min(axis=0)

        return smallest

    def gethighestpixel(self):
        """
        # Arguments
            dir             : The directory where the file(s) is found
        # Extra
            Treats it as an array of graph-points checks for highest values
        """

        img = self.img_array[:, :-1]
        biggest = img.max(axis=0).max(axis=0)

        return biggest

    def getaverage(self):
        """
        # Arguments
            dir             : The directory where the file(s) is found
        # Extra
            Treats it as an array of graph-points checks for average values
        """

        img = self.img_array[:, :-1]
        average = img.mean(axis=0).mean(axis=0)

        return int(average)

    def getData(self):
        return self.return_data

    def init(self):
        self.width = self.img_array.shape[1]
        self.height = self.img_array.shape[0]
        self.high = self.gethighestpixel()
        self.low = self.getlowestpixel()
        self.dominant = self.getdominant()
        self.avg = self.getaverage()
        if self.imageValidation():
            self.return_data = {
                'file_name': getLastDirectory(self.dir),
                'img_array': (self.img_array if not self.normalized else self.img_array / self.normalizeConst),
                'img_XY': [self.width, self.height],
                'img_prop': [(self.low if not self.normalized else self.low / self.normalizeConst),
                             (self.high if not self.normalized else self.high / self.normalizeConst),
                             (self.avg if not self.normalized else self.avg / self.normalizeConst),
                             (int(self.dominant[0]) if not self.normalized else self.dominant[0] / self.normalizeConst)]
                , 'img_tweaks': {
                    'sharpened': self.sharpened,
                    'compressed': self.compressed,
                    'normalized': self.normalized

                }
            }
        else:
            self.return_data = None

    def __init__(self, dir):
        self.dir = dir
        self.img_array = self.toArray()
        self.normalized = False
        self.sharpened = False
        self.compressed = False
        self.init()
