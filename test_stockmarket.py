import os

from libs.stockmarket.stockmarket import Stockmarket

# "Stockmarket" returns x_train, y_train || x_validation, y_validation
path = os.curdir
ret = Stockmarket(params={'data': path+'/libs/stockmarket/data/', 'rename_field_names':
    ['time', 'low', 'high', 'open', 'close', 'volume']}).load_merge()
print(ret)
